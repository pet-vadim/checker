module gitlab.com/pet-vadim/checker

go 1.17

require (
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.10.4
	github.com/rabbitmq/amqp091-go v1.3.0
	gitlab.com/pet-vadim/libs v1.0.9
)

require (
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.20.0 // indirect
)
