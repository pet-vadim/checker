package tester

import "context"

type Service interface {
	Ping(ctx context.Context, test *Test)
}
