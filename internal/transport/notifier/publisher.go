package notifierbroker

import (
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/pet-vadim/checker/internal/services/notifier"
	"gitlab.com/pet-vadim/libs/logger"
)

type publisher struct {
	amqpDial   *amqp.Connection
	amqpDialCh *amqp.Channel
	conf       *RabbitMq
}

func NewPublisher(conf *RabbitMq) notifier.Broker {
	amqpDial, err := amqp.Dial(conf.DataSource)
	failOnError(err, "fail dial to rabbitmq")

	ch, err := amqpDial.Channel()
	failOnError(err, "fail create channel on rabbitmq")

	_, err = ch.QueueDeclare(
		conf.QueueName,
		conf.Durable,
		conf.AutoDelete,
		conf.Exclusive,
		conf.NoWait,
		conf.Args,
	)
	failOnError(err, "failed to declare a queue")

	return &publisher{
		amqpDial:   amqpDial,
		amqpDialCh: ch,
		conf:       conf,
	}
}

func (p *publisher) Close() {
	if err := p.amqpDialCh.Close(); err != nil {
		logger.Error("consumer channel close err: " + err.Error())
	}

	if err := p.amqpDial.Close(); err != nil {
		logger.Error("consumer conn close err: " + err.Error())
	}
}

func failOnError(err error, msg string) {
	if err != nil {
		logger.Fatal(msg + " " + err.Error())
	}
}

type RabbitMq struct {
	DataSource  string
	QueueName   string
	Consumer    string
	Exchange    string
	DeliveryKey string
	Mandatory   bool
	Durable     bool
	AutoDelete  bool
	AutoAck     bool
	Exclusive   bool
	NoLocal     bool
	NoWait      bool
	Args        map[string]interface{}
}
